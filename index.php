<?php
    require_once("config.php");
    require_once(ROOT_PATH."/models/product.php");
    require_once(ROOT_PATH."/models/category.php");


    $products = getHomeProduct($pdo);
    $categories = getCategories($pdo);
    $tree = fetchCategoryTreeList($pdo);


    require_once(ROOT_PATH."/templates/home.php");

    



?>