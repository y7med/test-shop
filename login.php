<?php
    require_once("config.php");
    require_once(ROOT_PATH."/models/user.php");


    $errors = [];
    if(!empty($_POST)){
        if(empty($_POST['inputEmail'])){
            $errors[] =  "Please enter your Email";
        }
        if(empty($_POST['inputPassword'])){
            $errors[] =  "Please enter your Password";
        }
        if(empty($errors)){
            $login = checkLogin($pdo, SALT, $_POST['inputEmail'], $_POST['inputPassword']);
            if($login){
                $_SESSION['id'] = $login;
                //redirect
                header("Location:/index.php");
            }else {
                $errors[] = "Credentials are invalid"; 
            }
        }
    }
    

    require_once(ROOT_PATH."/templates/login.php");


?>
