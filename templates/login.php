<?php require_once(ROOT_PATH . "/templates/header.php"); ?>

<main role="main" class="inner cover">
  <h1 class="cover-heading">Log In</h1>
  <form class="form-signin" method="POST" action="<?php echo SITE_URL."/login.php" ?>">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <div>
      <?php foreach($errors as $error): ?>
      <div class="alert alert-danger" role="alert">
        <?php echo $error; ?>
      </div>
      <?php endforeach; ?>
    </div>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" name="inputEmail" id="inputEmail" class="form-control" value="<?php echo(!empty($_POST['inputEmail'])) ? $_POST['inputEmail'] :''; ?>" placeholder="Email address" required autofocus>
    <br/>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
    <br/>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
  </form>
</main>

<?php require_once(ROOT_PATH . "/templates/footer.php"); ?>