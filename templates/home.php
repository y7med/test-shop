<?php require_once(ROOT_PATH . "/templates/header.php"); ?>

<main role="main" class="inner cover mt-5">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <?php foreach($tree as $r): ?>
          <?php echo $r; ?>
        <?php endforeach; ?>
      </div>
      <div class="col-sm-8">
        <div class="row">
          <?php foreach($products as $product) :  ?>
            <div class="col-sm-6">
              <div class="card mb-4">
                <div><a href="<?php echo SITE_URL; ?>/product_details.php?id=<?php echo $product['id']; ?>"><img class="card-img-top" src="http://www.modernstationers.com/upload/product.png"></a></div>
                <div class="card-body">
                  <p class="card-text"><?php echo $product['title']; ?></p>
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">Order</button>
                    </div>
                    <small class="text-muted"><?php echo $product['price']; ?></small>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>



<?php require_once(ROOT_PATH . "/templates/footer.php"); ?>